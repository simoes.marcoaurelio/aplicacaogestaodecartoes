package br.com.microservicofatura;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MicroServicoFaturaApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicroServicoFaturaApplication.class, args);
	}

}
