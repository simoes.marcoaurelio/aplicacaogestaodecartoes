package br.com.microservicopagamento;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class MicroServicoPagamentoApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicroServicoPagamentoApplication.class, args);
	}

}
