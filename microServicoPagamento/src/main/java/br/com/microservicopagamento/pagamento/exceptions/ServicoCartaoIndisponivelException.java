package br.com.microservicopagamento.pagamento.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_GATEWAY, reason = "Serviço de consulta ao cartão indisponível")
public class ServicoCartaoIndisponivelException extends RuntimeException{
}
