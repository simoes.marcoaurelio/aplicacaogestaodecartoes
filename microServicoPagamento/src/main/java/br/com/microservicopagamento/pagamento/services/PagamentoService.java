package br.com.microservicopagamento.pagamento.services;

import br.com.microservicopagamento.pagamento.clients.CartaoClient;
import br.com.microservicopagamento.pagamento.dtos.Cartao;
import br.com.microservicopagamento.pagamento.exceptions.CartaoSemPagamentosException;
import br.com.microservicopagamento.pagamento.models.Pagamento;
import br.com.microservicopagamento.pagamento.repositories.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PagamentoService {

    @Autowired
    private PagamentoRepository pagamentoRepository;

    @Autowired
    private CartaoClient cartaoClient;

    public Pagamento inserirPagamento(Pagamento pagamento) {
        Cartao cartao = cartaoClient.consultarCartaoPeloId(pagamento.getIdCartao());

        pagamento.setIdCartao(cartao.getIdCartao());
        return pagamentoRepository.save(pagamento);
    }

    public List<Pagamento> consultarPagamentosDeUmCartao(int idCartao) {
        List<Pagamento> listaDePagamentos = pagamentoRepository.findAllByIdCartao(idCartao);

        if (listaDePagamentos.size() > 0) {
            return listaDePagamentos;
        }
        else {
            throw new CartaoSemPagamentosException();
        }
    }
}
