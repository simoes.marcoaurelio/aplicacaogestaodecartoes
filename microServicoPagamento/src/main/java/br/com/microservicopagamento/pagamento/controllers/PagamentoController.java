package br.com.microservicopagamento.pagamento.controllers;

import br.com.microservicopagamento.pagamento.dtos.PagamentoMapper;
import br.com.microservicopagamento.pagamento.dtos.RequisicaoInserirPagamento;
import br.com.microservicopagamento.pagamento.dtos.RespostaInserirPagamento;
import br.com.microservicopagamento.pagamento.models.Pagamento;
import br.com.microservicopagamento.pagamento.security.Usuario;
import br.com.microservicopagamento.pagamento.services.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
public class PagamentoController {

    @Autowired
    private PagamentoService pagamentoService;

    @Autowired
    private PagamentoMapper pagamentoMapper;

    @PostMapping("/pagamento")
    @ResponseStatus(HttpStatus.CREATED)
    public RespostaInserirPagamento inserirPagamento(
            @RequestBody @Valid RequisicaoInserirPagamento requisicaoInserirPagamento,
            @AuthenticationPrincipal Usuario usuario) {

// APENAS PARA TESTAR O AUTHENTICATIONPRINCIPAL
        System.out.println("Informaçeõs do cliente: id: " + usuario.getId() + " , name: " + usuario.getName());

        Pagamento pagamento = pagamentoMapper.paraPagamento(requisicaoInserirPagamento);
        pagamento = pagamentoService.inserirPagamento(pagamento);
        return pagamentoMapper.paraRespostaInserirPagamento(pagamento);
    }

    @GetMapping("/pagamentos/{id_cartao}")
    @ResponseStatus(HttpStatus.OK)
    public List<RespostaInserirPagamento> consultarPagamentosDeUmCartao(@PathVariable int id_cartao) {

        List<Pagamento> pagamentos = pagamentoService.consultarPagamentosDeUmCartao(id_cartao);

        List<RespostaInserirPagamento> listaDePagamentos = new ArrayList<>();
        for (Pagamento pagamento : pagamentos) {
            RespostaInserirPagamento respostaInserirPagamento =
                    pagamentoMapper.paraRespostaInserirPagamento(pagamento);
            listaDePagamentos.add(respostaInserirPagamento);
        }

        return listaDePagamentos;
    }
}
