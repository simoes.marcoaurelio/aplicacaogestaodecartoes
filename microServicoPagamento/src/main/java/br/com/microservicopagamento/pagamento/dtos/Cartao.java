package br.com.microservicopagamento.pagamento.dtos;

public class Cartao {

    private int idCartao;
    private String numeroCartao;
    private boolean cartaoAtivo;
    private int idCliente;

    public Cartao() {
    }

    public int getIdCartao() {
        return idCartao;
    }

    public void setIdCartao(int idCartao) {
        this.idCartao = idCartao;
    }

    public String getNumeroCartao() {
        return numeroCartao;
    }

    public void setNumeroCartao(String numeroCartao) {
        this.numeroCartao = numeroCartao;
    }

    public boolean isCartaoAtivo() {
        return cartaoAtivo;
    }

    public void setCartaoAtivo(boolean cartaoAtivo) {
        this.cartaoAtivo = cartaoAtivo;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }
}
