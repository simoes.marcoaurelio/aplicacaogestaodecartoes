package br.com.microservicopagamento.pagamento.clients;

import br.com.microservicopagamento.pagamento.exceptions.CartaoNaoEncontradoException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class CartaoClientDecoder implements ErrorDecoder{

    private ErrorDecoder errorDecoder = new ErrorDecoder.Default();

    @Override
    public Exception decode(String s, Response response) {
        if (response.status() == 404) {
            throw new CartaoNaoEncontradoException();
        }else{
            return errorDecoder.decode(s, response);
        }
    }
}
