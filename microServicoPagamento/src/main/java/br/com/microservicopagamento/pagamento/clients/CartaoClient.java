package br.com.microservicopagamento.pagamento.clients;

import br.com.microservicopagamento.pagamento.dtos.Cartao;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cartao", configuration = CartaoClientConfiguration.class)
public interface CartaoClient {

    @GetMapping("/cartao/consulta/{idCartao}")
    Cartao consultarCartaoPeloId(@PathVariable int idCartao);
}
