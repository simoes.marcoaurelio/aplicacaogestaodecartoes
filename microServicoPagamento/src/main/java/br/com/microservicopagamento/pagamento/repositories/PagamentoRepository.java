package br.com.microservicopagamento.pagamento.repositories;

import br.com.microservicopagamento.pagamento.models.Pagamento;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PagamentoRepository extends CrudRepository<Pagamento, Integer> {
    List<Pagamento> findAllByIdCartao(int idCartao);
}
