package br.com.microservicopagamento.pagamento.clients;

import br.com.microservicopagamento.pagamento.dtos.Cartao;
import br.com.microservicopagamento.pagamento.exceptions.ServicoCartaoIndisponivelException;

public class CartaoClientFallback implements CartaoClient {

    @Override
    public Cartao consultarCartaoPeloId(int idCartao) {

        throw new ServicoCartaoIndisponivelException();
    }
}
