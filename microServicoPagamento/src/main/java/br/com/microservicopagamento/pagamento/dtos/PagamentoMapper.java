package br.com.microservicopagamento.pagamento.dtos;

import br.com.microservicopagamento.pagamento.models.Pagamento;
import org.springframework.stereotype.Component;

@Component
public class PagamentoMapper {

    public Pagamento paraPagamento(RequisicaoInserirPagamento requisicaoInserirPagamento) {
        Pagamento pagamento = new Pagamento();
        pagamento.setDescricaoPagamento(requisicaoInserirPagamento.getDescricaoPagamento());
        pagamento.setValorPagamento(requisicaoInserirPagamento.getValorPagamento());
        pagamento.setIdCartao(requisicaoInserirPagamento.getIdCartao());

        return pagamento;
    }

    public RespostaInserirPagamento paraRespostaInserirPagamento(Pagamento pagamento) {
        RespostaInserirPagamento respostaInserirPagamento = new RespostaInserirPagamento();

        respostaInserirPagamento.setIdPagamento(pagamento.getIdPagamento());
        respostaInserirPagamento.setIdCartao(pagamento.getIdCartao());
        respostaInserirPagamento.setDescricaoPagamento(pagamento.getDescricaoPagamento());
        respostaInserirPagamento.setValorPagamento(pagamento.getValorPagamento());

        return respostaInserirPagamento;
    }
}
