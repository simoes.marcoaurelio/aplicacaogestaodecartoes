package br.com.microservicocartao.cartao.service;

import br.com.microservicocartao.cartao.clients.ClienteClient;
import br.com.microservicocartao.cartao.dtos.Cliente;
import br.com.microservicocartao.cartao.dtos.RequisicaoAtualizarCartao;
import br.com.microservicocartao.cartao.exceptions.CartaoJaCadastradoException;
import br.com.microservicocartao.cartao.exceptions.CartaoNaoEncontradoException;
import br.com.microservicocartao.cartao.models.Cartao;
import br.com.microservicocartao.cartao.repositories.CartaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService {

    @Autowired
    private CartaoRepository cartaoRepository;

    @Autowired
    private ClienteClient clienteClient;

    public void verificarCartaoExistente(String numeroCartao) {
        Optional<Cartao> optionalCartao = cartaoRepository.findByNumeroCartao(numeroCartao);
        if (optionalCartao.isPresent()) {
            throw new CartaoJaCadastradoException();
        }
    }

    public Cartao cadastrarCartao(Cartao cartao) {

        Cliente cliente = clienteClient.consultarClientePorId(cartao.getIdCliente());

        cartao.setIdCliente(cliente.getIdCliente());
        cartao.setCartaoAtivo(false);
        return cartaoRepository.save(cartao);
    }

    public Cartao atualizarStatusCartao(String numeroCartao, RequisicaoAtualizarCartao requisicaoAtualizarCartao) {

        Cartao cartao = consultarCartaoPeloNumero(numeroCartao);

        cartao.setCartaoAtivo(requisicaoAtualizarCartao.isCartaoAtivo());
        return cartaoRepository.save(cartao);
    }

    public Cartao consultarCartaoPeloNumero(String numeroCartao) {
        Optional<Cartao> optionalCartao = cartaoRepository.findByNumeroCartao(numeroCartao);

        if (optionalCartao.isPresent()) {
            return optionalCartao.get();
        }
        else {
            throw new CartaoNaoEncontradoException();
        }
    }

    public Cartao consultarCartaoPeloId(int idCartao) {
        Optional<Cartao> optionalCartao = cartaoRepository.findById(idCartao);

        if (optionalCartao.isPresent()) {
            return optionalCartao.get();
        }
        else {
            throw new CartaoNaoEncontradoException();
        }
    }
}
