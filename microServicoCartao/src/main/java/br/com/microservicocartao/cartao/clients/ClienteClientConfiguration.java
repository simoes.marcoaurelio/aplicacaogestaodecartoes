package br.com.microservicocartao.cartao.clients;

import feign.Feign;
import feign.RetryableException;
import feign.codec.ErrorDecoder;
import io.github.resilience4j.feign.FeignDecorators;
import io.github.resilience4j.feign.Resilience4jFeign;
import org.springframework.context.annotation.Bean;

public class ClienteClientConfiguration extends OAuth2FeignConfiguration{
//public class ClienteClientConfiguration {


// alteração de configuração para captura de erro retornado por outro microsserviço no feign, e tratamento para exibição
    @Bean
    public ErrorDecoder getErrorDecoder() {
        return new ClienteClientDecoder();
    }

// alteração de configuração para catura de erro no feign para fallback, erro de comunicação entre serviços
    @Bean
    public Feign.Builder builder() {
        FeignDecorators feignDecorators = FeignDecorators.builder()
                .withFallback(new ClienteClientFallback(), RetryableException.class)
                .build();

        return Resilience4jFeign.builder(feignDecorators);
    }
}
