package br.com.microservicocartao.cartao.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.CONFLICT, reason = "Número de cartão já existente")
public class CartaoJaCadastradoException extends RuntimeException{
}
