package br.com.microservicocartao.cartao.models;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
public class Cartao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idCartao;

    @Size(min = 9, max = 9, message = "O cartão deve ter 9 números")
    @Column(unique = true)
    private String numeroCartao;

    private boolean cartaoAtivo;

    private int idCliente;

    public Cartao() {
    }

    public int getIdCartao() {
        return idCartao;
    }

    public void setIdCartao(int idCartao) {
        this.idCartao = idCartao;
    }

    public String getNumeroCartao() {
        return numeroCartao;
    }

    public void setNumeroCartao(String numeroCartao) {
        this.numeroCartao = numeroCartao;
    }

    public boolean isCartaoAtivo() {
        return cartaoAtivo;
    }

    public void setCartaoAtivo(boolean cartaoAtivo) {
        this.cartaoAtivo = cartaoAtivo;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }
}
