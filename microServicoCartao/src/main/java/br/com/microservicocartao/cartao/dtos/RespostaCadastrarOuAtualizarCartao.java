package br.com.microservicocartao.cartao.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RespostaCadastrarOuAtualizarCartao {

    @JsonProperty("id")
    private int idCartao;

    @JsonProperty("numero")
    private String numeroCartao;

    @JsonProperty("clienteId")
    private int idCliente;

    @JsonProperty("ativo")
    private boolean cartaoAtivo;

    public RespostaCadastrarOuAtualizarCartao() {
    }

    public int getIdCartao() {
        return idCartao;
    }

    public void setIdCartao(int idCartao) {
        this.idCartao = idCartao;
    }

    public String getNumeroCartao() {
        return numeroCartao;
    }

    public void setNumeroCartao(String numeroCartao) {
        this.numeroCartao = numeroCartao;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public boolean isCartaoAtivo() {
        return cartaoAtivo;
    }

    public void setCartaoAtivo(boolean cartaoAtivo) {
        this.cartaoAtivo = cartaoAtivo;
    }
}
