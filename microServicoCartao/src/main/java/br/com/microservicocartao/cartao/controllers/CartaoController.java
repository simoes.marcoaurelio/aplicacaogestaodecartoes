package br.com.microservicocartao.cartao.controllers;

import br.com.microservicocartao.cartao.dtos.*;
import br.com.microservicocartao.cartao.models.Cartao;
import br.com.microservicocartao.cartao.security.Usuario;
import br.com.microservicocartao.cartao.service.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/cartao")
public class CartaoController {

    @Autowired
    private CartaoService cartaoService;

    @Autowired
    private CartaoMapper cartaoMapper;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public RespostaCadastrarOuAtualizarCartao cadastrarCartao(
            @RequestBody @Valid RequisicaoCadastrarCartao requisicaoCadastrarCartao,
            @AuthenticationPrincipal Usuario usuario) {
// APENAS PARA TESTAR O AUTHENTICATIONPRINCIPAL
        System.out.println("Informaçeõs do cliente: id: " + usuario.getId() + " , name: " + usuario.getName());

        cartaoService.verificarCartaoExistente(requisicaoCadastrarCartao.getNumeroCartao());

        Cartao cartao = cartaoMapper.paraCartao(requisicaoCadastrarCartao);
        cartao = cartaoService.cadastrarCartao(cartao);
        return cartaoMapper.paraRespostaCadastrarOuAtualizarCartao(cartao);
    }

    @PatchMapping("/{numero}")
    @ResponseStatus(HttpStatus.OK)
    public RespostaCadastrarOuAtualizarCartao atualizarStatusCartao(
            @PathVariable String numero, @RequestBody @Valid RequisicaoAtualizarCartao requisicaoAtualizarCartao) {

        Cartao cartao = cartaoService.atualizarStatusCartao(numero,requisicaoAtualizarCartao);
        return cartaoMapper.paraRespostaCadastrarOuAtualizarCartao(cartao);
    }

    @GetMapping("/{numero}")
    @ResponseStatus(HttpStatus.OK)
    public RespostaConsultarCartao consultarCartaoPeloNumero(@PathVariable String numero) {

        Cartao cartao = cartaoService.consultarCartaoPeloNumero(numero);
        return cartaoMapper.paraRespostaConsultarCartao(cartao);
    }

    @GetMapping("/consulta/{idCartao}")
    public Cartao consultarCartaoPeloId(@PathVariable int idCartao,
                                        @AuthenticationPrincipal Usuario usuario) {
// APENAS PARA TESTAR O AUTHENTICATIONPRINCIPAL
        System.out.println("Informaçeõs do cliente: id: " + usuario.getId() + " , name: " + usuario.getName());

        return cartaoService.consultarCartaoPeloId(idCartao);
    }
}
