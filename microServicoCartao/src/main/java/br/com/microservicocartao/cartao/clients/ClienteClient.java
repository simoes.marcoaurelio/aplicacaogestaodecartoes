package br.com.microservicocartao.cartao.clients;

import br.com.microservicocartao.cartao.dtos.Cliente;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

//name - configura a api que deverá ser chamada para buscar as informações desejadas
//configuration - ativa a possibilidade de alterar as configurações do client
//                (nesse caso, vamos usar para aletrar o decoder de erro)
@FeignClient(name = "cliente", url = "http://localhost:8080" ,configuration = ClienteClientConfiguration.class)
public interface ClienteClient {

    @GetMapping("/cliente/{id}")
    Cliente consultarClientePorId(@PathVariable int id);
}
