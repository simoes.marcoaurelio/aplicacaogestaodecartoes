package br.com.microservicocartao.cartao.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RespostaConsultarCartao {

    @JsonProperty("id")
    private int idCartao;

    @JsonProperty("numero")
    private String numeroCartao;

    @JsonProperty("clienteId")
    private int idCliente;

    public RespostaConsultarCartao() {
    }

    public int getIdCartao() {
        return idCartao;
    }

    public void setIdCartao(int idCartao) {
        this.idCartao = idCartao;
    }

    public String getNumeroCartao() {
        return numeroCartao;
    }

    public void setNumeroCartao(String numeroCartao) {
        this.numeroCartao = numeroCartao;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }
}