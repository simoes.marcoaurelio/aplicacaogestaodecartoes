package br.com.microservicocartao.cartao.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Cliente {

    @JsonProperty("id")
    private int idCliente;

    @JsonProperty("name")
    private String nomeCliente;

    public Cliente() {
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public String getNomeCliente() {
        return nomeCliente;
    }

    public void setNomeCliente(String nomeCliente) {
        this.nomeCliente = nomeCliente;
    }
}
