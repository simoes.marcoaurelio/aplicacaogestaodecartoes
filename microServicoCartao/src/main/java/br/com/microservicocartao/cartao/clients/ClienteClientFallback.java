package br.com.microservicocartao.cartao.clients;

import br.com.microservicocartao.cartao.dtos.Cliente;
import br.com.microservicocartao.cartao.exceptions.ServicoClienteIndisponivelException;

public class ClienteClientFallback implements ClienteClient {

    @Override
    public Cliente consultarClientePorId(int id) {

//        Cliente cliente = new Cliente();
//        cliente.setNomeCliente("Cliente erro");
//        cliente.setIdCliente(99);

//        return cliente;

        throw new ServicoClienteIndisponivelException();
    }
}
