package br.com.microservicocartao.cartao.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RequisicaoAtualizarCartao {

    @JsonProperty("ativo")
    private boolean cartaoAtivo;

    public RequisicaoAtualizarCartao() {
    }

    public boolean isCartaoAtivo() {
        return cartaoAtivo;
    }

    public void setCartaoAtivo(boolean cartaoAtivo) {
        this.cartaoAtivo = cartaoAtivo;
    }
}
