package br.com.microservicocartao.cartao.dtos;

import br.com.microservicocartao.cartao.models.Cartao;
import org.springframework.stereotype.Component;

@Component
public class CartaoMapper {

    public Cartao paraCartao(RequisicaoCadastrarCartao requisicaoCadastrarCartao) {
        Cartao cartao = new Cartao();
        cartao.setNumeroCartao(requisicaoCadastrarCartao.getNumeroCartao());
        cartao.setIdCliente(requisicaoCadastrarCartao.getIdCliente());

        return cartao;
    }

    public RespostaCadastrarOuAtualizarCartao paraRespostaCadastrarOuAtualizarCartao(Cartao cartao) {
        RespostaCadastrarOuAtualizarCartao respostaCadastrarOuAtualizarCartao =
                new RespostaCadastrarOuAtualizarCartao();

        respostaCadastrarOuAtualizarCartao.setIdCartao(cartao.getIdCartao());
        respostaCadastrarOuAtualizarCartao.setNumeroCartao(cartao.getNumeroCartao());
        respostaCadastrarOuAtualizarCartao.setIdCliente(cartao.getIdCliente());
        respostaCadastrarOuAtualizarCartao.setCartaoAtivo(cartao.isCartaoAtivo());

        return respostaCadastrarOuAtualizarCartao;
    }

    public RespostaConsultarCartao paraRespostaConsultarCartao(Cartao cartao) {
        RespostaConsultarCartao respostaConsultarCartao = new RespostaConsultarCartao();

        respostaConsultarCartao.setIdCartao(cartao.getIdCartao());
        respostaConsultarCartao.setNumeroCartao(cartao.getNumeroCartao());
        respostaConsultarCartao.setIdCliente(cartao.getIdCliente());

        return respostaConsultarCartao;
    }
}
