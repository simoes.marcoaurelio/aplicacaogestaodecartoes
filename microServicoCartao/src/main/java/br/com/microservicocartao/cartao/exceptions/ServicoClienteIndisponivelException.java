package br.com.microservicocartao.cartao.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_GATEWAY, reason = "Serviço de consulta ao cliente indisponível")
public class ServicoClienteIndisponivelException extends RuntimeException{
}
