package br.com.microservicocartao.cartao.clients;

import br.com.microservicocartao.cartao.exceptions.ClienteNaoEncontradoException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class ClienteClientDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if (response.status() == 404) {
            throw new ClienteNaoEncontradoException();
        }else{
            return errorDecoder.decode(s, response);
        }
    }
}
