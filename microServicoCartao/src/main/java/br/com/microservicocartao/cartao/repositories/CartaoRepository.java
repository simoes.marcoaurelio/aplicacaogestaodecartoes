package br.com.microservicocartao.cartao.repositories;

import br.com.microservicocartao.cartao.models.Cartao;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface CartaoRepository extends CrudRepository<Cartao, Integer> {
    Optional<Cartao> findByNumeroCartao(String numeroCartao);
}
