package br.com.microservicocartao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class MicroServicoCartaoApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicroServicoCartaoApplication.class, args);
	}

}
