package br.com.microservicocliente.cliente.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RespostaCliente {

    @JsonProperty("id")
    private int idCliente;

    @JsonProperty("name")
    private String nomeCliente;

    public RespostaCliente() {
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public String getNomeCliente() {
        return nomeCliente;
    }

    public void setNomeCliente(String nomeCliente) {
        this.nomeCliente = nomeCliente;
    }
}
