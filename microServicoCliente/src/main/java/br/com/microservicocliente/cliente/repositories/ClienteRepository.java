package br.com.microservicocliente.cliente.repositories;

import br.com.microservicocliente.cliente.models.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface ClienteRepository extends CrudRepository<Cliente, Integer> {
}
