package br.com.microservicocliente.cliente.dtos;

import br.com.microservicocliente.cliente.models.Cliente;
import org.springframework.stereotype.Component;

@Component
public class ClienteMapper {

    public Cliente paraCliente(RequisicaoCliente requisicaoCliente) {
        Cliente cliente = new Cliente();

        cliente.setNomeCliente(requisicaoCliente.getNomeCliente());
        return cliente;
    }

    public RespostaCliente paraRespostaCadastrarCliente(Cliente cliente){
        RespostaCliente respostaCliente = new RespostaCliente();

        respostaCliente.setIdCliente(cliente.getIdCliente());
        respostaCliente.setNomeCliente(cliente.getNomeCliente());
        return respostaCliente;
    }
}
