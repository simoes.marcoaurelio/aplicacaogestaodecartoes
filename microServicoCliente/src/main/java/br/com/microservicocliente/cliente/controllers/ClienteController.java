package br.com.microservicocliente.cliente.controllers;

import br.com.microservicocliente.cliente.dtos.ClienteMapper;
import br.com.microservicocliente.cliente.dtos.RequisicaoCliente;
import br.com.microservicocliente.cliente.dtos.RespostaCliente;
import br.com.microservicocliente.cliente.models.Cliente;
import br.com.microservicocliente.cliente.security.Usuario;
import br.com.microservicocliente.cliente.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    @Autowired
    private ClienteMapper clienteMapper;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public RespostaCliente cadastrarCliente(
            @RequestBody @Valid RequisicaoCliente requisicaoCliente, @AuthenticationPrincipal Usuario usuario) {
// APENAS PARA TESTAR O AUTHENTICATIONPRINCIPAL
        System.out.println("Informaçeõs do cliente: id: " + usuario.getId() + " , name: " + usuario.getName());

        try {
            Cliente cliente = clienteMapper.paraCliente(requisicaoCliente);
            cliente = clienteService.cadastrarCliente(cliente);
            return clienteMapper.paraRespostaCadastrarCliente(cliente);
        }catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public RespostaCliente consultarClientePorId(@PathVariable int id, @AuthenticationPrincipal Usuario usuario) {
// APENAS PARA TESTAR O AUTHENTICATIONPRINCIPAL
        System.out.println("Informaçeõs do cliente: id: " + usuario.getId() + " , name: " + usuario.getName());

        Cliente cliente = clienteService.consultarClientePorId(id);
        return clienteMapper.paraRespostaCadastrarCliente(cliente);
    }
}
