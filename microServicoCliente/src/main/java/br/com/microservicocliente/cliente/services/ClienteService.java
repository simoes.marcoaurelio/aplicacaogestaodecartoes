package br.com.microservicocliente.cliente.services;

import br.com.microservicocliente.cliente.exceptios.ErroAoConsultarClienteException;
import br.com.microservicocliente.cliente.models.Cliente;
import br.com.microservicocliente.cliente.repositories.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    public Cliente cadastrarCliente(Cliente cliente) {
        return clienteRepository.save(cliente);
    }

    public Cliente consultarClientePorId(int idCliente) {
        Optional<Cliente> optionalCliente = clienteRepository.findById(idCliente);

        if (optionalCliente.isPresent()) {
            return optionalCliente.get();
        }
        else {
            throw new ErroAoConsultarClienteException();
        }
    }
}