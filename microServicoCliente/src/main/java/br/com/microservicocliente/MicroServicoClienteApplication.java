package br.com.microservicocliente;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MicroServicoClienteApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicroServicoClienteApplication.class, args);
	}

}
